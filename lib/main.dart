import 'package:flutter/material.dart';
import 'package:fugi_riverpod/src/app.dart';

void main() {
  final myApps = MyApps();
  runApp(myApps);
}
